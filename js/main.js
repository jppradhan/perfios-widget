/**
 * Dummy search data
 */
var bankData = [
    {
        name: "ACME Bank, india",
        id: 1
    },
    {
        name: "ICICI Bank, india",
        id: 2
    },
    {
        name: "HDFC Bank, india",
        id: 3
    },
    {
        name: "SBI Bank, india",
        id: 4
    },
    {
        name: "Punjab Bank, india",
        id: 5
    },
    {
        name: "AXIS Bank, india",
        id: 6
    },
    {
        name: "ANDHRA Bank, india",
        id: 7
    },
    {
        name: "KOTAK Manindra Bank, india",
        id: 8
    }
];
/* =============  Dummy data end here ================== */



var PW = PW || {};
/**
 * Default options
 * @type {{}}
 */
PW.options = {
    stepCarousel: '.step-carousel',
    descriptionCarousel: '.desc-carousel',
    selectInstitute: '#select-institute',
    getStartButton: '#getstart',
    credentialSection: '#credentials',
    credentialForm: '#enter-credential',
    otpSection: '#otp',
    otpForm: '#otp-form',
    captchaSection: '#captcha',
    captchaForm: '#captcha-form',
    accountSection: '.account',
    updatePasswordSection: '#updatepassword',
    updatePassForm: '#update-pass',
    refreshIcon: '.icon-refresh',
    instituteInput: '#select-institute',
    searchList: '.search-list',
    currentSelected: 0
}

/**
 * Application starts here
 */
PW.init = function() {
    this.stepCarousel();
    this.featureCarousel();
    this.showLoginFormOnChange();
    this.getStarted();
    this.submitCredential();
    this.submitOTP();
    this.submitCAPTCHA();
    this.addCustomValidation();
    this.closeMessages();
    this.refreshCaptcha();
    this.bindKeyPressToInstitute();
    this.moveInput();
    this.bindKeyDownKeyUp();
}
/**
 * Step owl carousel render
 */
PW.stepCarousel = function() {
    var carousel = $(this.options.stepCarousel).owlCarousel({
        nav:true,
        items: 1,
        dots: true,
        navText: ['<span class="left-arrow"></span>', '<span class="right-arrow"></span>']
    });
}
/**
 * Feture carousel render
 */
PW.featureCarousel = function() {
    var fcarousel = $(this.options.descriptionCarousel).owlCarousel({
        nav:true,
        items: 1,
        dots: false,
        navText: ['<span class="left-arrow"></span>', '<span class="right-arrow"></span>'],
        autoHeight: false
    });
}
/**
 * Show hide login form on changing the institution drop down
 */
PW.showLoginFormOnChange = function (val) {
    var $form = $(this.options.credentialForm);
    if(val) {
       $form.find('.form-wrap').fadeIn(400, function () {
           $form.find('.loading-delay').show();
       });
       $form.find('.start-linking').hide();
    } else {
       $form.find('.loading-delay').fadeOut(400);
       $form.find('.form-wrap').fadeOut(400, function () {
           $form.find('.start-linking').show();
       });
    }

}
/**
 * Add event listener
 */
PW.getStarted = function () {
    $(this.options.getStartButton).on('click', function(e) {
        e.preventDefault();
        $(this).parents('.get-started').hide();
        $('#credentials').fadeIn(400);
    });
}
/**
 * Enter credential submit
 */
PW.submitCredential = function() {
    //console.log(this.options.credentialForm, this.options.otpSection);
    var $credentialForm = $(this.options.credentialForm);
    var $otp = $(this.options.otpSection);

    $credentialForm.validate({
        rules: {
            institute: {
                required: true
            },
            loginid: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                sameAsPassword: '#password',
            }
        },
        submitHandler: function () {
            this.onSubmitCredential($credentialForm, $otp);
        }.bind(this)
    });
}
/**
 * Update password
 */
PW.submitUpdatePassword = function() {
    //console.log(this.options.credentialForm, this.options.otpSection);
    var $updateForm = $(this.options.updatePassForm);
    $updateForm.validate({
        rules: {
            loginid: {
                required: true,
                minlength: 2
            },
            password: {
                required: true,
                minlength: 5
            },
            cpassword: {
                required: true,
                sameAsPassword: '#updatepass',
            }
        },
        submitHandler: function () {
            this.onUpdatePassword();
        }.bind(this)
    });
}
/**
 * OTP Submit form
 */
PW.submitOTP = function() {
    var $otpForm = $(this.options.otpForm);
    var $captcha = $(this.options.captchaSection);
    $otpForm.validate({
        rules: {
            otp: {
                required: true,
                minlength: 3
            }
        },
        submitHandler: function () {
            this.onSubmitOTP($otpForm, $captcha);
        }.bind(this)
    });
}
/**
 * CAPTCHA Submit form
 */
PW.submitCAPTCHA = function () {
    var $capthcaForm = $(this.options.captchaForm);
    var $account = $(this.options.accountSection);
    $capthcaForm.validate({
        rules: {
            captcha: {
                required: true
            }
        },
        submitHandler: function () {
           this.onSubmitCaptch($capthcaForm, $account);
        }.bind(this)
    });
}
/**
 * Add custom validation message
 */
PW.addCustomValidation = function () {

    $.validator.addMethod("sameAsPassword", function(value, element, params) {
        if($(params).val().trim() != value.trim()) {
            return false;
        }
        return true;
    }, "Password and Confirm password not matching");
}
/**
 * close message events
 */
PW.closeMessages = function () {
    $(document).on('click', '.close', function(e) {
       e.preventDefault();
       $(this).parents('.message').fadeOut(400);
    });
    $(document).on('click', '.icon-close', function(e) {
        e.preventDefault();
        $(this).parents('.loading-delay').fadeOut(400);
    });
}
/**
 * refresh captcha functionality
 */
PW.refreshCaptcha = function () {
    $(this.options.refreshIcon).on('click', function() {
       console.log('Refreshed the captcha');
    });
}
/**
 * On submitting the credential form
 */
PW.onSubmitCredential = function ($credentialForm, $otp) {
   // Do something useful here
    $credentialForm.parents('.credentials').hide();
    $otp.fadeIn(400);
}
/**
 * on submit update password form
 */
PW.onUpdatePassword = function() {
    console.log('Password updated');
}
/**
 * on submitting the otp form
 * @param $otpForm
 * @param $captcha
 */
PW.onSubmitOTP = function ($otpForm, $captcha) {
    // Do something useful
    $otpForm.parents('.otp').hide();
    $captcha.fadeIn(400);
}
/**
 * On submit captcha form
 * @param $capthcaForm
 * @param $account
 */
PW.onSubmitCaptch = function ($capthcaForm, $account) {
    $capthcaForm.parents('.captcha').hide();
    $account.fadeIn(400);
}

PW.searchInstitute = function (val) {
    var results;
    results = bankData.filter(function(entry) {
        return entry.name.toUpperCase().indexOf(val.toUpperCase()) != -1;
    });
    var searchListElem = $(PW.options.searchList);
    var searchOverlayElem = $('.search-overlay');
    if(results.length == 1 && results[0].name == val.trim()) {
        return;
    }
    if(results.length) {
        var html = '';
        for(var i = 0;i < results.length;i++) {
            html = html + '<li><a class="'+ ((i == 0) ? 'active' : '') +'" href="javascript:void(0);" id="bankid-'+ results[i].id +'">'+ results[i].name +'</a></li>';
        }
        searchListElem.html(html);
        searchListElem.show();
        searchOverlayElem.show();
    } else {
        searchListElem.hide();
        searchOverlayElem.hide();
    }

    //bankData
}
/**
 * To search inititute on key press
 */
PW.bindKeyPressToInstitute = function () {
    var searchText = debounce(function (e) {
        var keyCode = e.which || e.keyCode;
        if((keyCode == 38) || (keyCode == 40) || (keyCode == 13)) {
            return;
        }
        var searchVal = $(this.options.instituteInput).val();
        PW.searchInstitute(searchVal);
    }.bind(this), 300);

    $(this.options.instituteInput).off().on('keyup', searchText);
    $(this.options.instituteInput).on('click', function() {
        PW.searchInstitute($(this).val());
    });
    $(PW.options.searchList).on('click', 'li a', function(e) {
        $(PW.options.instituteInput).val($(this).text().trim());
        PW.showLoginFormOnChange($(PW.options.instituteInput).val());
        $(PW.options.instituteInput).trigger('focusin');
        $(PW.options.instituteInput).trigger('blur');
        $(PW.options.searchList).hide();
        $('.search-overlay').hide();
        PW.options.currentSelected = 0;
    });
}
/**
 * Key event for arrow key up and down
 * for Dropdown list on institute input
 */
PW.bindKeyDownKeyUp = function () {
    if(isMobile()) {
        return;
    }
    $(PW.options.instituteInput).on('keydown', function (e) {
        var keyCode = (e.which || e.keyCode);
        if(keyCode == 38 || keyCode == 40) {
            var listLength = $(PW.options.searchList + ' li').length;
            if((listLength - 1 > PW.options.currentSelected) || (PW.options.currentSelected >= 0)) {
                var $active = $(PW.options.searchList + ' li:eq('+ PW.options.currentSelected +') a');
                $active.removeClass('active');
            }
        }
        var selected = '';
        var searchList = $(PW.options.searchList);

        function makeActive() {
            selected = $(PW.options.searchList + ' li:eq('+ PW.options.currentSelected +') a');
            selected.addClass('active');
            $(PW.options.instituteInput).val(selected.text().trim());

            if(selected.offset().top > searchList.height()) {
                searchList.scrollTop(searchList.scrollTop() + 50);
            }
            if(selected.offset().top < searchList.offset().top) {
                searchList.scrollTop(searchList.scrollTop() - 50);
            }
        }

        switch(keyCode) {
            case 38:
                e.preventDefault();
                if(PW.options.currentSelected >= 0) {
                    PW.options.currentSelected--;
                    makeActive();
                }
                break;
            case 40:
                e.preventDefault();
                if(listLength - 1 > PW.options.currentSelected) {
                    PW.options.currentSelected++;
                    makeActive();
                }
                break;
            case 13:
                e.preventDefault();
                var text = $(PW.options.searchList + ' li:eq('+ PW.options.currentSelected +') a').text().trim();
                $(PW.options.instituteInput).val(text);
                PW.showLoginFormOnChange($(PW.options.instituteInput).val());
                searchList.hide();
                $('.search-overlay').hide();
                break;
            default:
                return true;
        }
    });
}
/**
 * Move the input field on mobile devices on focus
 */
PW.moveInput = function() {
    var input = $('.otp-wrapper input');
    if(isMobile()) {
        input.off().on('focus', function() {
            $(this).parents('.otp-wrapper').css({
                '-webkit-transform' : 'translateY(-40%)',
                '-moz-transform'    : 'translateY(-40%)',
                '-ms-transform'     : 'translateY(-40%)',
                '-o-transform'      : 'translateY(-40%)',
                'transform'         : 'translateY(-40%)'
            });
        })
        .on('blur', function() {
            $(this).parents('.otp-wrapper').css({
                '-webkit-transform' : 'translateY(0)',
                '-moz-transform'    : 'translateY(0)',
                '-ms-transform'     : 'translateY(0)',
                '-o-transform'      : 'translateY(0)',
                'transform'         : 'translateY(0)'
            });
        });
    }
}
/**
 * Debounce function for typing
 * @param func
 * @param wait
 * @param immediate
 * @returns {Function}
 */
function debounce(func, wait, immediate) {
    var timeout;
    return function() {
        var context = this, args = arguments;
        var later = function() {
            timeout = null;
            if (!immediate) func.apply(context, args);
        };
        var callNow = immediate && !timeout;
        clearTimeout(timeout);
        timeout = setTimeout(later, wait);
        if (callNow) func.apply(context, args);
    };
};
/** ========================  http://stackoverflow.com/questions/11381673/detecting-a-mobile-browser **/
function isMobile() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
}

$(document).ready(function(){
    PW.init();
});